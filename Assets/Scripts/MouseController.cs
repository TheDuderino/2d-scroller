﻿using UnityEngine;

public class MouseController : MonoBehaviour {
	
	public float _jetpackForce = 75.0f;
	public float _forwardMovementSpeed = 3.0f;
	
	public Transform _groundCheckTransform;
	public LayerMask _groundCheckLayerMask;
	Animator _animator;	

	private bool _grounded = true;
    private bool _dead = false;

    public ParticleSystem jetpack;     //attached in unity gui    
    public Texture2D coinIconTexture;  //attached in unity gui

    private uint coins = 0;

    // Use this for initialization
    void Start () {
		_animator = GetComponent<Animator>();
        _animator.SetBool("dead", false);
        _animator.SetBool("grounded", true);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnGUI()
    {
        DisplayCoinsCount();
        DisplayRestartButton();
    }

    // Called each frame
    void FixedUpdate() 
    {
        bool jetpackActive = Input.GetButton("Fire1");
 
        if (jetpackActive && !_dead)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, _jetpackForce));
        }
		
        if (!_dead)
        { 
		    Vector2 newVelocity = GetComponent<Rigidbody2D>().velocity;
		    newVelocity.x = _forwardMovementSpeed;
		    GetComponent<Rigidbody2D>().velocity = newVelocity;
        }

        UpdateGroundedStatus();
        AdjustJetpack(jetpackActive);
    }

    void AdjustJetpack(bool active)
    {
        var em = jetpack.emission;
        em.enabled = !_grounded;

        jetpack.emissionRate = active ? 300.0f : 0.0f;
    }

	void UpdateGroundedStatus()
	{
		_grounded = Physics2D.OverlapCircle(_groundCheckTransform.position, 0.1f, _groundCheckLayerMask);
		_animator.SetBool("grounded", _grounded);
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Coins"))
            CollectCoin(collider);
        else
            HitByLaser(collider);
    }

    void CollectCoin(Collider2D coinCollider)
    {
        coins++;
        Destroy(coinCollider.gameObject);
    }

    void HitByLaser(Collider2D laserCollider)
    {
		if (!_dead)
			laserCollider.gameObject.GetComponent<AudioSource>().Play();

        //_jetpack.Pause(); // somehow one of these seem to thow an exception?
        //_jetpack.Clear(); // somehow one of these seem to thow an exception?
        _dead = true;
        _animator.SetBool("dead", true);
    }

    void DisplayCoinsCount()
    {
        Rect coinIconRect = new Rect(10, 10, 32, 32);
        GUI.DrawTexture(coinIconRect, coinIconTexture);

        GUIStyle style = new GUIStyle();
        style.fontSize = 30;
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.yellow;

        Rect labelRect = new Rect(coinIconRect.xMax, coinIconRect.y, 60, 32);
        GUI.Label(labelRect, coins.ToString(), style);
    }

    void DisplayRestartButton()
    {
        if (_dead && _grounded)
        {
            Rect restartButton = new Rect(Screen.width * 0.35f, Screen.height * 0.45f, Screen.width * 0.30f, Screen.height * 0.1f);
          
            if (GUI.Button(restartButton, "Tap to restart!"))
            {
                Application.LoadLevel(Application.loadedLevelName);
            };
        }
    }

}
